package com.mariusz.dao;

import com.mariusz.dao.generic.TransactionalGenericDAO;
import com.mariusz.model.Student;

/**
 * Created by Wojak on 10.02.14.
 */
public interface StudentDAO extends TransactionalGenericDAO<Student, Long> {
}
