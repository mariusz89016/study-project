package com.mariusz.dao.generic;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import com.googlecode.genericdao.search.ExampleOptions;
import com.googlecode.genericdao.search.Filter;
import com.googlecode.genericdao.search.ISearch;
import com.googlecode.genericdao.search.SearchResult;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

public interface TransactionalGenericDAO<T, ID extends Serializable> extends GenericDAO<T, ID> {
    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public T find( ID id );

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public T[] find( ID... ids );

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public T getReference( ID id );

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public T[] getReferences( ID... ids );

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public boolean save( T entity );

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public boolean[] save( T... entities );

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public boolean remove( T entity );

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public void remove( T... entities );

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public boolean removeById( ID id );

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public void removeByIds( ID... ids );

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public List<T> findAll( );

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public <RT> List<RT> search( ISearch search );

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public <RT> RT searchUnique( ISearch search );

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public int count( ISearch search );

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public <RT> SearchResult<RT> searchAndCount( ISearch search );

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public boolean isAttached( T entity );

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public void refresh( T... entities );

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public void flush( );

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public Filter getFilterFromExample( T example );

    @Override
    @Transactional(propagation = Propagation.MANDATORY)
    public Filter getFilterFromExample( T example, ExampleOptions options );
}
