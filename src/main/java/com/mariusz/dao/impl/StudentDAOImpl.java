package com.mariusz.dao.impl;

import com.mariusz.dao.StudentDAO;
import com.mariusz.dao.generic.TransactionalGenericDAOImpl;
import com.mariusz.model.Student;
import org.springframework.stereotype.Repository;

/**
 * Created by Wojak on 10.02.14.
 */
@Repository
public class StudentDAOImpl extends TransactionalGenericDAOImpl<Student, Long> implements StudentDAO {
}
