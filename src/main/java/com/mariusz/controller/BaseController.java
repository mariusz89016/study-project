package com.mariusz.controller;

import com.mariusz.model.Student;
import com.mariusz.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Wojak
 */
@Controller
@RequestMapping("/")
public class BaseController {

    @Autowired
    @Qualifier("studentServiceImpl")
    private StudentService studentService;


    @RequestMapping(value="/", method = RequestMethod.GET)
    public String link(ModelMap model) {
        model.addAttribute("message", "<a href=\"/study_project/welcome\">LINK!</a>");
        return "index";
    }
    @RequestMapping(value="/welcome", method = RequestMethod.GET)
    public String welcome(ModelMap model) {
        model.addAttribute("message", "Hello World!");
        return "index";
    }

    @RequestMapping(value="/student", method = RequestMethod.GET)
    public String student(ModelMap model) {
        Student student = new Student();
        student.setFirstname("Jan");
        student.setLastname("Nowak");

        model.addAttribute("firstname", student.getFirstname());
        model.addAttribute("lastname", student.getLastname());

        studentService.create(student);
        return "student";
    }
}
