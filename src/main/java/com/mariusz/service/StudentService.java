package com.mariusz.service;

import com.mariusz.model.Student;
import com.mariusz.service.generic.CRUDService;

/**
 * Created by Wojak on 11.02.14.
 */
public interface StudentService extends CRUDService<Student, Long> {
}
