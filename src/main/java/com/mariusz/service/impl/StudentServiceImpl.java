package com.mariusz.service.impl;

import com.mariusz.dao.StudentDAO;
import com.mariusz.model.Student;
import com.mariusz.service.StudentService;
import com.mariusz.service.generic.CRUDServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentServiceImpl extends CRUDServiceImpl<Student, Long> implements StudentService {

    @Autowired
    private StudentDAO studentDAO;

    public StudentDAO getDAO() {
        return studentDAO;
    }
}
