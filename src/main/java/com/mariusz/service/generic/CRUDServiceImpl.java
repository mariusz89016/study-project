package com.mariusz.service.generic;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

@Service
public abstract class CRUDServiceImpl<T, ID extends Serializable> implements CRUDService<T, ID> {

    @Transactional(propagation = Propagation.REQUIRED)
    public T create(T t) {
        getDAO().save(t);
        return t;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public T read(ID id) {
        return getDAO().find(id);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public T update(T t) {
        getDAO().save(t);
        return t;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void delete(T t) {
        getDAO().remove(t);
    }
}