package com.mariusz.service.generic;

import com.mariusz.dao.generic.TransactionalGenericDAO;

import java.io.Serializable;

public interface CRUDService<T, ID extends Serializable> {
    public T create(T t);
    public T read(ID id);
    public T update(T t);
    public void delete(T t);
    public TransactionalGenericDAO<T, ID> getDAO();
}
